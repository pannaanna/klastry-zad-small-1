import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.util.Set;
import java.util.HashSet;

public class JaccardSimilarity {
    private static String input1 = new String("tweets1.csv");
    private static String input2 = new String("tweets2.csv");
    private static String outputDir = new String("tweets-output/");
    private static String fileName = new String("jaccard_similarities.txt");
    private static Integer kShingle = 3;

    private static Set<String> getAllKShingles(String text) {
        Set<String> shingles = new HashSet<String>();
        for (int i = 0; i < text.length() - (kShingle - 1); i++) {
            shingles.add(text.substring(i, i + kShingle));
        }
        return shingles;
    }

    private static double jaccardSimilarity(String content1, String content2) {
        Set<String> kShingles1 = getAllKShingles(content1);
        Set<String> kShingles2 = getAllKShingles(content2);
        Set<String> intersection = new HashSet<String>(kShingles1);
        intersection.retainAll(kShingles2);
        Set<String> union = new HashSet<String>(kShingles1);
        union.addAll(kShingles2);
        return (double) intersection.size() / union.size();
    }

    private static String getTweetContent(String line) {
        String[] splitted = line.split(",");
        return splitted[1].replaceAll("\"", "");
    }

    private static double processLines(String line1, String line2) {
        String content1 = getTweetContent(line1);
        String content2 = getTweetContent(line2);
        return jaccardSimilarity(content1, content2);
    }

    public static void main(String[] args) throws Exception {
        String hdfsUserDir = new String(args[0]);
        Path path1 = new Path(hdfsUserDir + input1);
        Path path2 = new Path(hdfsUserDir + input2);

        Configuration conf = new Configuration();
        conf.addResource(new Path("/tmp/hadooop-ap/hadoop-2.8.3/etc/hadoop/core-site.xml"));
        conf.addResource(new Path("/tmp/hadooop-ap/hadoop-2.8.3/etc/hadoop/hdfs-site.xml"));
        //conf.set("fs.defaultFS", hdfsUri);
        //System.setProperty("HADOOP_USER_NAME", "hdfs");

        FileSystem fs = FileSystem.get(conf);

        Path workingDir=fs.getWorkingDirectory();
        Path newFolderPath= new Path(hdfsUserDir + outputDir);
        if(!fs.exists(newFolderPath)) {
            fs.mkdirs(newFolderPath);
        }

        Path writePath = new Path(newFolderPath + "/" + fileName);
        FSDataOutputStream outputStream = fs.create(writePath);

        FSDataInputStream inputStream1 = fs.open(path1);
        FSDataInputStream inputStream2 = fs.open(path2);
        BufferedReader br1 = new BufferedReader(new InputStreamReader(inputStream1));
        BufferedReader br2 = new BufferedReader(new InputStreamReader(inputStream2));

        String line1 = br1.readLine();
        String line2 = br2.readLine();
        try {
            do {
                double jaccardIndex = processLines(line1, line2);
                outputStream.writeBytes(String.valueOf(jaccardIndex));
                outputStream.writeBytes("\n");
                line1 = br1.readLine();
                line2 = br2.readLine();
            } while (line1 != null && line2 != null);
        } finally {
            br1.close();
            br2.close();
            outputStream.close();
        }
    }
}
