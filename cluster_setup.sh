# sudo apt install ssh
# sudo apt install rsync
# java -version (install if needed)

. config.sh

install_script=install_hadoop.sh

# Passwordless SSH setup for slaves:

# ssh-keygen -t rsa -b 4096
# while read slave; do
# ssh-copy-id -i $HOME/.ssh/id_rsa.pub $user@$slave
# done <${slaves_file}

### Install hadoop on namenode and datanodes

bash ${install_script}

while read slave; do
    ssh $user@$slave "mkdir -p $hadoop_dir"
    scp config.sh $user@$slave:$hadoop_dir
    scp ${install_script} $user@$slave:$hadoop_dir
    ssh $user@$slave "cd $hadoop_dir; bash $install_script"
    scp ${slaves_file} $user@$slave:${hadoop_conf_dir}slaves
done <${slaves_file}

### Format cluster

${hadoop_pref}/bin/hdfs namenode -format -nonInteractive

while read slave; do
    ssh $user@$slave "mkdir -p $namenode_dir"
    scp -r $namenode_dir $user@$slave:${hdfs_data_dir}
done <${slaves_file}
