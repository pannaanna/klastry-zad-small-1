. config.sh

$hadoop_pref/sbin/stop-dfs.sh
$hadoop_pref/sbin/stop-yarn.sh
$hadoop_pref/sbin/yarn-daemon.sh --config $hadoop_conf_dir stop proxyserver
$hadoop_pref/sbin/mr-jobhistory-daemon.sh --config $hadoop_conf_dir stop historyserver
