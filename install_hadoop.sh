. config.sh

mkdir $hadoop_dir
cd $hadoop_dir

# only if necesary
wget -N http://ftp.man.poznan.pl/apache/hadoop/common/${hadoop_version}/${hadoop_version}.tar.gz
# TODO verify pgp keys

tar xvzf ${hadoop_version}.tar.gz
cd ${hadoop_version}/

sed -i -e "s|^export JAVA_HOME=\${JAVA_HOME}|export JAVA_HOME=${java_home}|g" etc/hadoop/hadoop-env.sh

echo "<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://${master}:9000</value>
    </property>
</configuration>" > etc/hadoop/core-site.xml

echo "
<configuration>
    <property>
            <name>dfs.namenode.name.dir</name>
            <value>${namenode_dir}</value>
    </property>

    <property>
            <name>dfs.datanode.data.dir</name>
            <value>${datanode_dir}</value>
    </property>

    <property>
            <name>dfs.replication</name>
            <value>1</value>
    </property>
</configuration>" > etc/hadoop/hdfs-site.xml
