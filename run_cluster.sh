### Run cluster

. config.sh 

$hadoop_pref/sbin/start-dfs.sh
$hadoop_pref/sbin/start-yarn.sh
$hadoop_pref/sbin/mr-jobhistory-daemon.sh --config $hadoop_pref/etc/hadoop start historyserver
$hadoop_pref/bin/hdfs dfs -mkdir /user
$hadoop_pref/bin/hdfs dfs -mkdir /user/$user
