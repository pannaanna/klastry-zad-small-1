# Make sure cluster is running.

. config.sh

#upload_dir=uploaded
#dir=/user/$user/${upload_dir}

for file in "$@"
do
    #${hadoop_pref}/bin/hdfs dfs -mkdir $dir
    ${hadoop_pref}/bin/hdfs dfs -put $file $(basename $file)
done
